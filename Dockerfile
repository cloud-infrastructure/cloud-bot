FROM ubuntu

RUN apt-get update
RUN apt-get -y install expect redis-server nodejs npm

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN ln -s /usr/bin/nodejs /usr/bin/node

RUN npm install -g coffee-script
RUN npm install -g yo generator-hubot

# create hubot user
RUN useradd -d /hubot -m -s /bin/bash -U hubot

# change to hubot user
USER    hubot
WORKDIR /hubot

# install hubit
RUN yo hubot --owner="cloud devs <cloud-developers@cern.ch>" --name="nimbus" --description="nimbus the cloud bot" --defaults

# add adapters and scripts
RUN npm install hubot-xmpp --save && npm install
RUN npm install hubot-youtube --save && npm install
RUN npm install hubot-gitlab-hooks --save && npm install

# activate scripts
ADD hubot-scripts.json /hubot/
ADD external-scripts.json /hubot/
ADD scripts/hubot-jira-issues.coffee /hubot/scripts/
ADD scripts/hubot-gitlab.coffee /hubot/scripts/

EXPOSE 8080

CMD bin/hubot -a xmpp
