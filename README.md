## Overview

Nimbus, a hubot for the cloud. Runs in a docker container using the 'nimbus' service account.

## Launching

Probably want to run this from some VM (we're using nimbus.cern.ch right now).

Replace the password below twice (jabber and jira).

```
ssh root@nimbus.cern.ch
yum install -y docker
sudo docker run -d --name hubot --net=host -p 8080:8080/tcp -e HUBOT_XMPP_HOST=conference.jabber.cern.ch -e HUBOT_XMPP_PORT=5222 -e HUBOT_XMPP_USERNAME=nimbus@jabber.cern.ch -e HUBOT_XMPP_PASSWORD=<password-here> -e HUBOT_XMPP_ROOMS='cloud-infrastructure@conference.jabber.cern.ch' -e HUBOT_JIRA_URL=https://its.cern.ch/jira -e HUBOT_JIRA_USERNAME=nimbus -e HUBOT_JIRA_PASSWORD=<password-here> -e HUBOT_YOUTUBE_API_KEY=<youtube-apikey-here> -e GITLAB_CHANNEL='cloud-infrastructure@conference.jabber.cern.ch' -e GITLAB_DEBUG=1 -e HUBOT_LOG_LEVEL=debug rochaporto/hubot
```

## Enabling gitlab hooks

Under the project you want to enable this for, go to Settings, Web Hooks and add http://nimbus.cern.ch:8080/gitlab/web for 'Merge Request' events (or whatever you want).

## Extending

Follow one of the recipes below to update the Dockerfile, then rebuild and push the updated docker image:
```
docker build -t rochaporto/hubot .
docker push rochaporto/hubot
```

Relaunch the container, wherever it's running.
```
docker rm -f hubot
docker run ... (see above)
```

### Adding a new script from npm:
```
vim Dockerfile
# add adapters and scripts
RUN npm install hubot-<something> --save && npm install

vim external-scripts.json
[
...
	"hubot-<something>"
]
```

### Adding a new downloaded script

If you got the coffeescript file, add it directly:
```
vim Dockerfile

ADD scripts/hubot-jira-issues.coffee /hubot/scripts/
ADD scripts/MYNEWSCRIPT.coffee /hubot/scripts
```

